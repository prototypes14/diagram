# Prototype

```
Prototype of an application to demonstrate my skills. A solution consisting tool for  
- Project, Office Management 
- Automate Casino Games 
- RGS, Manage Games, Report Generation 
- Operator Lobby and Player Management
- Banking Application   
```

## Different Modules 
> Office Management   
> Project Manangement   
> Test Game Math  
> Create Game Client  
> Lobby And Operator 
> Gaming Server  
> Data Analysis  
> Reporting Tool  
> Stream Advt Videos  
> Bank 


## HLD
![Diagram](Prototype-HLD.jpeg)

## Flow 
![Flow](Prototype-LLD.png)


